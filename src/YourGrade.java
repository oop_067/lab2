import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score : ");

        if (sc.hasNextInt()) {

            int score = sc.nextInt();

            if (score >= 0 && score <= 100) {

                String grade = Grade(score);
                System.out.printf("Grade : %s", grade);

            } else {

                System.out.println("Please enter an integer between 0-100.");

            }

        } else {

            System.out.println("Please enter an integer value.");
        }
        
        sc.close();

    }

    public static String Grade(int score) {

        if (score >= 80) {
            return "A";
        } else if (score < 80 && score >= 75) {
            return "B+";
        } else if (score < 75 && score >= 70) {
            return "B";
        } else if (score < 70 && score >= 65) {
            return "C+";
        } else if (score < 65 && score >= 60) {
            return "C";
        } else if (score < 60 && score >= 55) {
            return "D+";
        } else if (score < 55 && score >= 50) {
            return "D";
        } else {
            return "F";
        }
    }

}